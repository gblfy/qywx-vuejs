## 企业微信自自建内部应用
前端项目部署手册
```shell
# 1.克隆前端项目
git clone git@gitee.com:gblfy/qywx-vuejs.git

# 2.安装依赖
cd qywx-vuejs
npm install --registry=https://registry.npm.taobao.org

# 3.运行项目
npm run dev
```
企业微信文档：
https://developer.work.weixin.qq.com/document/path/91039
![img.png](pic/img.png)

企业微信管控台：https://work.weixin.qq.com/wework_admin/loginpage_wx
![img_1.png](pic/img_1.png)

![img.png](pic/img_2.png)

